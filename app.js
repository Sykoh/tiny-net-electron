const electron = require("electron");
const app = electron.app;
const globalShortcut = electron.globalShortcut;
const BrowserWindow = electron.BrowserWindow;
const ipc = electron.ipcMain;
const path = require('path');
const url = require('url');
const widevine = require('electron-widevinecdm');

// widevineCDM needs to start running before the ready event 
widevine.loadAsync(app)
  .then(() => {
    // if widevineCDM is loaded after the app is ready, the user needs to relaunch the app; 
    if (app.isReady()) {
      app.relaunch();
    }
});

let mainScreen;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let navWin;
 
let appState;

function createWindow(){
    // Create the browser window.
    win = new BrowserWindow({ 
        width: 800, 
        height: 600, 
        frame: false,
        webPreferences: {
            plugins: true,
            sandbox: true,
            nodeIntegration: false
        }
    });

    appState = new AppState(win);

    ipc.on("navigate", (event, message) => {
        if(!message.startsWith("http://") || !message.startsWith("http://")){
            message = "http://" + message;
        }

        win.loadURL(message);
        if(navWin){
            navWin.close();
            navWin = null;
        }
    });

    ipc.on("closeNavWin", (event, message) => {
        if(navWin){
            navWin.close();
            navWin = null;
        }
    });

    win.loadURL("https://www.netflix.com");

    // Open the DevTools.
    //win.webContents.openDevTools();

    const screen = electron.screen;
    mainScreen = screen.getPrimaryDisplay();

    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });

    // Register a 'CommandOrControl+Y' shortcut listener.
    globalShortcut.register('Alt+S', () => {
        appState.toggleVideoMode();
    });

    globalShortcut.register('Alt+=', () => {
        appState.scaleUp();
    });

    globalShortcut.register('Alt+-', () => {
        appState.scaleDown();
    });

    globalShortcut.register('Alt+up', () => {
        if(appState.isVideoMode())
            appState.moveVideoMode(0, -0.5);
    });

    globalShortcut.register('Alt+down', () => {
        if(appState.isVideoMode())
            appState.moveVideoMode(0, 0.5);
    });

    globalShortcut.register('Alt+left', () => {
        if(appState.isVideoMode())
            appState.moveVideoMode(-0.5, 0);
    });

    globalShortcut.register('Alt+right', () => {
        if(appState.isVideoMode())
            appState.moveVideoMode(0.5, 0);
    });

    globalShortcut.register('Alt+esc', () => {
        win.close();
    });

    globalShortcut.register('Alt+X', () => {
        appState.toggleHideShow();
    });

    globalShortcut.register('Alt+N', () => {
        if(!navWin){
            navWin = new BrowserWindow({ resizable: false, width: 400, height: 250 });
            navWin.setMenu(null);
            navWin.loadURL(path.join(__dirname, "assets/navbar.html"));
            navWin.on("closed", function(){
                navWin = null;
            });
        }
        else{
            navWin.close();
            navWin = null;
        }
    });

    let ratios = [];
    globalShortcut.register('Alt+A', () => {
        // Aspect ratios
        appState.toggleAspectRatio();
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

function AppState(window){
    this.win = window;

    let isVideoMode = false;
    let browserBaseWidth = 800;
    let browserBaseHeight = 600;
    let browserScaleFactor = 1.0;

    let videoModeBaseWidth = 400;
    let videoModeBaseHeight = 400;
    let videoModeScaleFactor = 1.0;
    let videoModeAspectRatioX = 1;
    let videoModeAspectRatioY = 0.625;
    let videoModePosX = 1.0;
    let videoModePosY = 0.5;

    let isHidden = false;

    this.isHidden = function(){
        return isHidden;
    }

    this.isVideoMode = function(){
        return isVideoMode;
    }

    this.toggleVideoMode = function(){
        isVideoMode = !isVideoMode;
        
        if(isVideoMode)
            this.setVideoMode();
        else
            this.setBrowserMode();
    }

    this.setVideoMode = function(){
        win.setMovable(false);
        win.setAlwaysOnTop(true);
        
        let width = Math.round(videoModeBaseWidth * videoModeAspectRatioX * videoModeScaleFactor);
        let height = Math.round(videoModeBaseHeight * videoModeAspectRatioY * videoModeScaleFactor);

        setSize(width, height);
        setPosition(width, height, videoModePosX, videoModePosY);
    }

    this.setBrowserMode = function(){
        win.setMovable(true);
        win.setAlwaysOnTop(false);
        
        let width = Math.round(browserBaseWidth * browserScaleFactor);
        let height = Math.round(browserBaseHeight * browserScaleFactor);

        setSize(width, height);
        win.setPosition(Math.round((mainScreen.size.width / 2) - (width / 2)), Math.round((mainScreen.size.height / 2) - (height / 2)));
    }

    let setSize = function(width, height){
        if(width > mainScreen.size.width)
            width = mainScreen.size.width;
        if(height > mainScreen.size.height)
            height = mainScreen.size.height;

        win.setSize(width, height);
    }

    this.scaleUp = function(){
        if(isVideoMode){
            videoModeScaleFactor += 0.1;
            this.setVideoMode();
        }
        else{
            browserScaleFactor += 0.1;
            this.setBrowserMode();
        }
    }

    this.scaleDown = function(){
        if(isVideoMode){
            videoModeScaleFactor -= 0.1;
            this.setVideoMode();
        }
        else{
            browserScaleFactor -= 0.1;
            this.setBrowserMode();
        }
    }

    this.moveVideoMode = function(x, y){
        let moveX = videoModePosX + x;
        let moveY = videoModePosY + y;
        if(moveX <= 1 && moveX >= 0){
            videoModePosX = moveX;
        }
        if(moveY <= 1 && moveY >= 0){
            videoModePosY = moveY;
        }

        let width = Math.round(videoModeBaseWidth * videoModeAspectRatioX * videoModeScaleFactor);
        let height = Math.round(videoModeBaseHeight * videoModeAspectRatioX * videoModeScaleFactor);
        
        setPosition(width, height, videoModePosX, videoModePosY);
    }

    function setPosition(width, height, posX, posY){
        let pixelPosX = Math.round((mainScreen.size.width * posX) - (width * posX));
        let pixelPosY = Math.round((mainScreen.size.height * posY) - (height * posY));

        win.setPosition(pixelPosX, pixelPosY);
    }

    this.toggleHideShow = function(){
        
        if(isHidden){
            win.show();
            win.setSkipTaskbar(false);
        }
        else{
            win.hide();
            win.setSkipTaskbar(true);
        }

        win.webContents.sendInputEvent({type: "keyDown", keyCode: "Space"});
        win.webContents.sendInputEvent({type: "keyUp", keyCode: "Space"});
        isHidden = !isHidden;
    }

    this.toggleAspectRatio = function(){
        if(videoModeAspectRatioY === 0.75){
            // 16:9
            videoModeAspectRatioY = 0.5625;
        }
        else if(videoModeAspectRatioY === 0.5625){
            // 16:10
            videoModeAspectRatioY = 0.625;
        }
        else if(videoModeAspectRatioY === 0.625){
            // 4:3
            videoModeAspectRatioY = 0.75;
        }

        this.setVideoMode();
    }
}