"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
var path = require("path");
var fs = require("fs");
var widevine = require("electron-widevinecdm");
var video_mode_state_1 = require("./video-mode-state");
widevine.loadAsync(electron_1.app)
    .then(function () {
    if (electron_1.app.isReady()) {
        electron_1.app.relaunch();
    }
});
var win;
var videoModeState = new video_mode_state_1["default"]();
var isHidden = false;
var isBrowserMode = true;
var settingsPath = path.join(__dirname, "video-settings.json");
if (!fs.existsSync(settingsPath)) {
    fs.writeFileSync(settingsPath, "{}");
}
var settings = JSON.parse(fs.readFileSync(settingsPath).toString());
videoModeState.setState(settings);
var browserSettingsPath = path.join(__dirname, "browser-settings.json");
if (!fs.existsSync(browserSettingsPath)) {
    fs.writeFileSync(browserSettingsPath, "{ \"width\": 800, \"height\": 600 }");
}
var browserSettings = JSON.parse(fs.readFileSync(browserSettingsPath).toString());
var browserState = { width: browserSettings.width, height: browserSettings.height };
var mainScreen;
var resolution;
var init = function () {
    mainScreen = electron_1.screen.getPrimaryDisplay();
    resolution = new video_mode_state_1.Vector(mainScreen.size.width, mainScreen.size.height);
    videoModeState.setScreenResolution(resolution);
    createWindow();
};
var createWindow = function () {
    win = new electron_1.BrowserWindow({
        width: browserSettings.width,
        height: browserSettings.height,
        frame: false,
        resizable: true
    });
    win.loadURL(path.join(__dirname, "assets", "index.html"));
    win.webContents.openDevTools();
    win.on("resize", function () {
        if (isBrowserMode) {
            browserState.width = win.getBounds().width;
            browserState.height = win.getBounds().height;
        }
    });
    win.on('closed', function () {
        win = null;
        fs.writeFileSync(path.join(__dirname, "video-settings.json"), JSON.stringify(videoModeState.getState()));
        fs.writeFileSync(path.join(__dirname, "browser-settings.json"), JSON.stringify(browserState));
    });
    electron_1.globalShortcut.register('Alt+V', function () {
        isBrowserMode = !isBrowserMode;
        if (isBrowserMode) {
            win.setAlwaysOnTop(false);
            win.setResizable(true);
            win.setSize(Math.floor(browserState.width), Math.floor(browserState.height));
            var x = Math.floor((resolution.x / 2) - (browserState.width / 2));
            var y = Math.floor((resolution.y / 2) - (browserState.height / 2));
            win.setPosition(x, y);
        }
        else {
            win.setAlwaysOnTop(true);
            win.setResizable(false);
            var position = videoModeState.getPixelPosition();
            win.setPosition(position.x, position.y);
            win.setSize(videoModeState.getPixelWidth(), videoModeState.getPixelHeight());
        }
    });
    electron_1.globalShortcut.register('Alt+=', function () {
        if (isBrowserMode)
            return;
        videoModeState.scaleUp();
        var position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
        win.setSize(videoModeState.getPixelWidth(), videoModeState.getPixelHeight());
    });
    electron_1.globalShortcut.register('Alt+-', function () {
        if (isBrowserMode)
            return;
        videoModeState.scaleDown();
        var position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
        win.setSize(videoModeState.getPixelWidth(), videoModeState.getPixelHeight());
    });
    electron_1.globalShortcut.register('Alt+up', function () {
        if (isBrowserMode)
            return;
        videoModeState.moveUp();
        var position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });
    electron_1.globalShortcut.register('Alt+down', function () {
        if (isBrowserMode)
            return;
        videoModeState.moveDown();
        var position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });
    electron_1.globalShortcut.register('Alt+left', function () {
        if (isBrowserMode)
            return;
        videoModeState.moveLeft();
        var position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });
    electron_1.globalShortcut.register('Alt+right', function () {
        if (isBrowserMode)
            return;
        videoModeState.moveRight();
        var position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });
    electron_1.globalShortcut.register('Alt+esc', function () {
        win.close();
    });
    electron_1.globalShortcut.register('Alt+X', function () {
        if (isBrowserMode)
            return;
        if (isHidden) {
            win.show();
            win.setSkipTaskbar(false);
        }
        else {
            win.hide();
            win.setSkipTaskbar(true);
        }
        win.webContents.sendInputEvent({ type: "keyDown", keyCode: "Space" });
        win.webContents.sendInputEvent({ type: "keyUp", keyCode: "Space" });
        isHidden = !isHidden;
    });
    electron_1.globalShortcut.register('Alt+N', function () {
        win.webContents.send("toggle");
    });
    var ratios = [];
    electron_1.globalShortcut.register('Alt+A', function () {
        if (isBrowserMode)
            return;
        videoModeState.toggleAspectRatio();
    });
};
electron_1.app.on("ready", function () {
    init();
});
//# sourceMappingURL=app.js.map