"use strict";
exports.__esModule = true;
var VideoModeState = (function () {
    function VideoModeState() {
        this.width = 480;
        this.sizeScaler = 1;
        this.aspectRatioIndex = 0;
        this.aspectRatioSet = [{ x: 16, y: 10 }, { x: 16, y: 9 }, { x: 4, y: 3 }];
        this.position = new Vector(1, 1);
        this.screenResolution = new Vector(1000, 1000);
    }
    VideoModeState.prototype.setScreenResolution = function (resolution) {
        this.screenResolution = resolution;
    };
    VideoModeState.prototype.setScaler = function (scaler) {
        this.sizeScaler = scaler;
    };
    VideoModeState.prototype.getScaler = function () {
        return this.sizeScaler;
    };
    VideoModeState.prototype.scaleUp = function () {
        this.sizeScaler += 0.1;
    };
    VideoModeState.prototype.scaleDown = function () {
        if (this.sizeScaler > 0.3) {
            this.sizeScaler -= 0.1;
        }
    };
    VideoModeState.prototype.getPixelWidth = function () {
        return Math.floor(this.width * this.sizeScaler);
    };
    VideoModeState.prototype.getPixelHeight = function () {
        var currentAspectRatio = this.aspectRatioSet[this.aspectRatioIndex];
        var actualWidth = this.width * this.sizeScaler;
        var delta = actualWidth / currentAspectRatio.x;
        return Math.floor(delta * currentAspectRatio.y);
    };
    VideoModeState.prototype.toggleAspectRatio = function () {
        this.aspectRatioIndex++;
        if (this.aspectRatioIndex >= this.aspectRatioSet.length)
            this.aspectRatioIndex = 0;
    };
    VideoModeState.prototype.setPosition = function (position) {
        if (position.x > 2 || position.x < 0 || position.y > 2 || position.y < 0)
            return;
        this.position = position;
    };
    VideoModeState.prototype.moveUp = function () {
        if (this.position.y > 0)
            this.position.y -= 1;
    };
    VideoModeState.prototype.moveDown = function () {
        if (this.position.y < 2)
            this.position.y += 1;
    };
    VideoModeState.prototype.moveLeft = function () {
        if (this.position.x > 0)
            this.position.x -= 1;
    };
    VideoModeState.prototype.moveRight = function () {
        if (this.position.x < 2)
            this.position.x += 1;
    };
    VideoModeState.prototype.getPixelPosition = function () {
        var width = this.getPixelWidth();
        var height = this.getPixelHeight();
        var offsetX = (width / 2) * this.position.x;
        var offsetY = (height / 2) * this.position.y;
        var screenDeltaX = (this.screenResolution.x / 2) * this.position.x;
        var screenDeltaY = (this.screenResolution.y / 2) * this.position.y;
        var result = new Vector(screenDeltaX - offsetX, screenDeltaY - offsetY);
        return result;
    };
    VideoModeState.prototype.getState = function () {
        var state = {};
        state.sizeScaler = this.sizeScaler;
        state.aspectRatioIndex = this.aspectRatioIndex;
        state.position = this.position;
        return state;
    };
    VideoModeState.prototype.setState = function (state) {
        if (state.sizeScaler)
            this.sizeScaler = state.sizeScaler;
        if (state.aspectRatioIndex)
            this.aspectRatioIndex = state.aspectRatioIndex;
        if (state.position) {
            if (state.position.hasOwnProperty("x"))
                this.position.x = state.position.x;
            if (state.position.hasOwnProperty("y"))
                this.position.y = state.position.y;
        }
    };
    return VideoModeState;
}());
exports["default"] = VideoModeState;
var Vector = (function () {
    function Vector(x, y) {
        this.x = x;
        this.y = y;
    }
    return Vector;
}());
exports.Vector = Vector;
//# sourceMappingURL=video-mode-state.js.map