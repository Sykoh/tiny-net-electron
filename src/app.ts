import { app, globalShortcut, BrowserWindow, ipcMain as ipc, screen, BrowserView, Event as ElectronEvent, ipcMain } from "electron";
import * as path from "path";
import * as url from "url";
import * as fs from "fs";
import * as widevine from "electron-widevinecdm";
import VideoModeState, { Vector } from "./video-mode-state";

// widevineCDM needs to start running before the ready event 
widevine.loadAsync(app)
.then(() => {
  // if widevineCDM is loaded after the app is ready, the user needs to relaunch the app; 
  if (app.isReady()) {
    app.relaunch();
  }
});

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: BrowserWindow;

let videoModeState = new VideoModeState();

// Running props
let isHidden = false;
let isBrowserMode = true;

// Create settings if does not exist
const settingsPath = path.join(__dirname, "video-settings.json");
if(!fs.existsSync(settingsPath)){
  fs.writeFileSync(settingsPath, "{}");
}

const settings = JSON.parse(fs.readFileSync(settingsPath).toString());
videoModeState.setState(settings);

const browserSettingsPath = path.join(__dirname, "browser-settings.json");
if(!fs.existsSync(browserSettingsPath)){
  fs.writeFileSync(browserSettingsPath, "{ \"width\": 800, \"height\": 600 }");
}

const browserSettings = JSON.parse(fs.readFileSync(browserSettingsPath).toString());

let browserState = { width: browserSettings.width, height: browserSettings.height };

let mainScreen;
let resolution;

const init = () => {
  // Set resolution
  mainScreen = screen.getPrimaryDisplay();
  resolution = new Vector(mainScreen.size.width, mainScreen.size.height);
  videoModeState.setScreenResolution(resolution);

  createWindow();
};

const createWindow = () => {
    // Create the browser window.
    win = new BrowserWindow({ 
        width: browserSettings.width, 
        height: browserSettings.height, 
        frame: false,
        resizable: true
        // webPreferences: {
        //     plugins: true,
        //     sandbox: true,
        //     nodeIntegration: true
        // }
    });

    win.loadURL(path.join(__dirname, "assets", "index.html"));

    // Open the DevTools.
    //win.webContents.openDevTools();

    // Emitted when the window is closed.
    win.on("resize", () => {
        if(isBrowserMode){
            browserState.width = win.getBounds().width;
            browserState.height = win.getBounds().height;
        }
    });

    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;

        fs.writeFileSync(path.join(__dirname, "video-settings.json"), JSON.stringify(videoModeState.getState()));
        
        fs.writeFileSync(path.join(__dirname, "browser-settings.json"), JSON.stringify(browserState));
    });

    // Register a 'CommandOrControl+Y' shortcut listener.
    globalShortcut.register('Alt+V', () => {
        isBrowserMode = !isBrowserMode;

        if(isBrowserMode){
            win.setAlwaysOnTop(false);
            win.setResizable(true);

            win.setSize(Math.floor(browserState.width), Math.floor(browserState.height));
            
            let x = Math.floor((resolution.x / 2) - (browserState.width / 2));
            let y = Math.floor((resolution.y / 2) - (browserState.height / 2));
            win.setPosition(x, y);
        }
        else{
            win.setAlwaysOnTop(true);
            win.setResizable(false);

            let position = videoModeState.getPixelPosition();
            win.setPosition(position.x, position.y);
            win.setSize(videoModeState.getPixelWidth(), videoModeState.getPixelHeight());
        }
    });

    globalShortcut.register('Alt+=', () => {
        if(isBrowserMode)
            return;

        videoModeState.scaleUp();
        let position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
        win.setSize(videoModeState.getPixelWidth(), videoModeState.getPixelHeight());
    });

    globalShortcut.register('Alt+-', () => {
        if(isBrowserMode)
            return;

        videoModeState.scaleDown();
        let position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
        win.setSize(videoModeState.getPixelWidth(), videoModeState.getPixelHeight());
    });

    globalShortcut.register('Alt+up', () => {
        if(isBrowserMode)
            return;

        videoModeState.moveUp();
        let position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });

    globalShortcut.register('Alt+down', () => {
        if(isBrowserMode)
            return;

        videoModeState.moveDown();
        let position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });

    globalShortcut.register('Alt+left', () => {
        if(isBrowserMode)
            return;

        videoModeState.moveLeft();
        let position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });

    globalShortcut.register('Alt+right', () => {
        if(isBrowserMode)
            return;

        videoModeState.moveRight();
        let position = videoModeState.getPixelPosition();
        win.setPosition(position.x, position.y);
    });

    globalShortcut.register('Alt+esc', () => {
        win.close();
    });

    globalShortcut.register('Alt+X', () => {
        if(isBrowserMode)
        return;

        if(isHidden){
            win.show();
            win.setSkipTaskbar(false);
        }
        else{
            win.hide();
            win.setSkipTaskbar(true);
        }

        win.webContents.sendInputEvent({type: "keyDown", keyCode: "Space"} as any);
        win.webContents.sendInputEvent({type: "keyUp", keyCode: "Space"} as any);
        isHidden = !isHidden;
    });

    globalShortcut.register('Alt+N', () => {
        win.webContents.send("toggle");
    });

    let ratios = [];
    globalShortcut.register('Alt+A', () => {
        if(isBrowserMode)
            return;

        // Aspect ratios
        videoModeState.toggleAspectRatio();
    });
};


app.on("ready", () => {
    init();
});
