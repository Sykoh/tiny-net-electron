export default class VideoModeState
{
    // A const width that everything is calculated from
    private readonly width: number = 480;

    // The scaler that is used when calculating width and height of the app
    private sizeScaler: number = 1;

    // The current aspect ratio index used to keep track of the current index;
    private aspectRatioIndex: number = 0;

    // The preset aspect ratio options
    private aspectRatioSet: Array<Vector> = [{ x: 16, y: 10 }, { x: 16, y: 9 }, { x: 4, y: 3 }];

    // Position of the window (0 - 2)
    private position: Vector;

    private screenResolution: Vector;

    constructor(){
        this.position = new Vector(1, 1);
        this.screenResolution = new Vector(1000, 1000);
    }

    setScreenResolution(resolution: Vector){
        this.screenResolution = resolution;
    }

    setScaler(scaler: number){
        this.sizeScaler = scaler;
    }

    getScaler(): number{
        return this.sizeScaler;
    }

    scaleUp(){
        this.sizeScaler += 0.1;
    }

    scaleDown(){
        if(this.sizeScaler > 0.3){
            this.sizeScaler -= 0.1;
        }
    }

    getPixelWidth(): number{
        return Math.floor(this.width * this.sizeScaler);
    }

    getPixelHeight(): number{
        const currentAspectRatio = this.aspectRatioSet[this.aspectRatioIndex];

        const actualWidth = this.width * this.sizeScaler;
        const delta = actualWidth / currentAspectRatio.x;
        return Math.floor(delta * currentAspectRatio.y);
    }

    // Changed the aspect ratio to the next variant
    toggleAspectRatio(){
        this.aspectRatioIndex++;
        if(this.aspectRatioIndex >= this.aspectRatioSet.length)
            this.aspectRatioIndex = 0;
    }

    setPosition(position: Vector){
        if(position.x > 2 || position.x < 0 || position.y > 2 || position.y < 0)
            return;
        
        this.position = position;
    }

    moveUp(){
        if(this.position.y > 0)
            this.position.y -= 1;
    }

    moveDown(){
        if(this.position.y < 2)
            this.position.y += 1;
    }

    moveLeft(){
        if(this.position.x > 0)
            this.position.x -= 1;
    }

    moveRight(){
        if(this.position.x < 2)
            this.position.x += 1;
    }

    getPixelPosition(): Vector {
        const width = this.getPixelWidth();
        const height = this.getPixelHeight();

        const offsetX = (width / 2) * this.position.x;
        const offsetY = (height / 2) * this.position.y;

        const screenDeltaX = (this.screenResolution.x / 2) * this.position.x;
        const screenDeltaY = (this.screenResolution.y / 2) * this.position.y;

        const result = new Vector(screenDeltaX - offsetX, screenDeltaY - offsetY);

        return result;
    }

    getState(): any{
        let state: any = {};
        state.sizeScaler = this.sizeScaler;
        state.aspectRatioIndex = this.aspectRatioIndex;
        state.position = this.position;
        
        return state;
    }

    setState(state: any){
        if(state.sizeScaler)
            this.sizeScaler = state.sizeScaler;

        if(state.aspectRatioIndex)
            this.aspectRatioIndex = state.aspectRatioIndex;
        
        if(state.position){
            if(state.position.hasOwnProperty("x"))
                this.position.x = state.position.x;
            
            if(state.position.hasOwnProperty("y"))
                this.position.y = state.position.y;
        }
    }
}

export class Vector{
    constructor(public x: number, public y: number){}
}