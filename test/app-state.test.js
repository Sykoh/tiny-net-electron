"use strict";
exports.__esModule = true;
var app_state_1 = require("../src/app-state");
var expect = require("expect");
describe("AppState", function () {
    it("should have the right pixel output for the aspect ratios", function () {
        var appState = new app_state_1["default"]();
        // 16:10
        expect(appState.getPixelWidth()).toBe(480);
        expect(appState.getPixelHeight()).toBe(300);
        // 16:9
        appState.toggleAspectRatio();
        expect(appState.getPixelWidth()).toBe(480);
        expect(appState.getPixelHeight()).toBe(270);
        // 4:3
        appState.toggleAspectRatio();
        expect(appState.getPixelWidth()).toBe(480);
        expect(appState.getPixelHeight()).toBe(360);
        // 16:10 again
        appState.toggleAspectRatio();
        expect(appState.getPixelWidth()).toBe(480);
        expect(appState.getPixelHeight()).toBe(300);
    });
    it("should calculate the pixel position of the window correctly", function () {
        var appState = new app_state_1["default"]();
        appState.setScreenResolution({ x: 1200, y: 1000 });
        // With aspect ratio 16:10 (480x300)
        appState.setPosition({ x: 0, y: 0 });
        var result = appState.getPixelPosition();
        expect(result.x).toBe(0);
        expect(result.y).toBe(0);
        appState.setPosition({ x: 1, y: 1 });
        result = appState.getPixelPosition();
        expect(result.x).toBe(360);
        expect(result.y).toBe(350);
        appState.setPosition({ x: 2, y: 2 });
        result = appState.getPixelPosition();
        expect(result.x).toBe(720);
        expect(result.y).toBe(700);
    });
    it("should set and retrieve state", function () {
        var state = {
            sizeScaler: 2,
            aspectRatioIndex: 1,
            position: {
                x: 1,
                y: 2
            },
            screenResolution: {
                x: 1200,
                y: 1000
            }
        };
        var appState = new app_state_1["default"]();
        appState.setState(state);
        var newState = appState.getState();
        expect(newState).toEqual(state);
    });
});
